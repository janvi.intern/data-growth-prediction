#!/bin/bash 

apt-get update
apt-get install python3
apt-get install python3-pip
sudo apt update
pip install --upgrade pip
apt install python3-venv
mkdir notebook
cd notebook
python3 -m venv jupyterenv
source jupyterenv/bin/activate
pip install jupyter
curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
echo "deb https://repos.influxdata.com/ubuntu bionic stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
apt update
apt install influxdb
systemctl start influxdb
influx
sed -i 's/# auth-enabled = false/auth-enabled = true/g' /etc/influxdb/influxdb.conf 
systemctl restart influxdb
cd jupyterenv
git clone https://gitlab.com/janvi.intern/data-growth-prediction.git
pip install pandas
pip install influxdb
pip install influxdb-client
pip install matplotlib
pip install plotly
pip install fpdf
apt install python3-dev
pip install pystan==2.19.1.1
pip install prophet
pip install fbprophet
