<h4><b>How to install all the dependencies and setup this repository with the help of bash script</b></h4>

Download the setup folder

<ul>
<li>
Run the bash script
    
    bash data_growth_prediction_setup_bash_script.sh

</li>

When the Script will start , at one point it will ask to configure the influxdb. 

<li>
Create a user in the influxdb using

    CREATE USER user_name WITH PASSWORD 'EXAMPLE_PASSWORD' WITH ALL PRIVILEGES

</li>
<li>
Exit from the influxdb shell with

    exit

</li>

Once you configure the influxdb , The Script will install all the Dependencies required. 

<h5>Once the Script is completely executed, Run the jupyter notebook </h5>

	jupyter notebook

<h5> Run the ipynb inside the jupyter notebook , Create atleast one one or two continuous queries while running for the first time and get your predicted values and graphs. </h5>

<h5><b> Note: The System Should have at least 4GB of memory to install prophet, and at least 2GB of memory to use prophet. </b></h5>


