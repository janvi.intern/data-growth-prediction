# Setup

<h4><b>Install and setup python on your System.</b></h4>

Check if python is already present or not:<br>
<ul>
<li>To check if python is installed on the System or not, Run 
    
    python3 -V

</li>

If python is not present on your system, run the following commands to install python & pip<br>

<li>
To install the latest version
    
    sudo apt-get install python3
    
</li>
<li>To install a particular version

    sudo apt-get install python[version-number]

</li><br>
Python is installed on the system now.

<h4><b>Install pip </b></h4> 

<li>
To install pip run the following command 

    sudo apt install python3-pip

</li>
<li>
Once the installation is complete, verify the installation by checking the pip version

    pip -V

</li>
<br>
For the project, we now require jupyter and influxdb to be installed on the system. 

<h4><b>Installing Jupyter</b></h4>

Run the following commands to install jupyter on the system<br>
<h5><b>Upgrade pip</b></h5>

<li>
Update all the packages

    sudo apt update

</li>
<li>
Upgrade the pip version
    
    pip install --upgrade pip

</li>
<br>

<h5><b>Install and Setup Virtual Environment</b></h5>

<li>
Install virtual environment

    sudo apt install python3-venv

</li>
<li>
Create a Directory  

    mkdir directory-name    

</li>
<li>
cd into the created directory 

    cd directory-name

</li>

<h5><b>Setup Virtual Environment in the created directory</b></h5>

<li>
Create a Virtual Environment

    python3 -m venv <venv-name>

</li>
<li>
Activate the Virtual Environment

    source <venv-name>/bin/activate

</li>

After Activating the Created Virtual Environment, Install Jupyter.

<h5><b>Install Jupyter</b></h5>

<li>
Installation

    pip install jupyter

</li>

<h4><b>Installing influxdb</b></h4>

<h5><b>Installation</b></h5>

<li>
Download and add the InfluxDB GPG key into your server.

    sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -

</li>
<li>
Add a new influxdb.list repository file to the list of available packages by executing the following command.

    sudo echo "deb https://repos.influxdata.com/ubuntu bionic stable" | sudo tee /etc/apt/sources.list.d/influxdb.list

</li>
<li>
Update the package information index

    sudo apt update

</li>
<li>
Install the influxdb package

    sudo apt install influxdb
    sudo apt install influxdb-client

</li>
<li>
Once installed, start the influxdb service.

    sudo systemctl start influxdb

</li>
<li>
Check the status of the InfluxDB server by running the command below

    sudo systemctl status influxdb

</li>

<h5><b>Configure The Influxdb Server</b></h5>

<li>
Start the influx shell by using the command

    influx

</li>
<li>
Create an influxdb account

    CREATE USER user_name WITH PASSWORD 'EXAMPLE_PASSWORD' WITH ALL PRIVILEGES

</li>
<li>
Quit the shell

    quit

</li>

<h5><b>Enable Authentication</b></h5>

<li>
Modifying the main InfluxDB configuration file
    
    sudo nano /etc/influxdb/influxdb.conf 

</li>

Locate the line # auth-enabled = false under the [http] section.

Change the value of auth-enabled from false to true and remove the leading # symbol from the line to uncomment the setting as shown below.   

Save and close the file. 

<li>
Next, restart the influxdb service to load the new security setting.
    
    sudo systemctl restart influxdb

</li>
<li>    
Whenever there is a need For logging in into the Influxdb Server . Use
    
    influx --username 'user_name' --password 'EXAMPLE_PASSWORD'

</li>

<h4><b>Clone the Github Repository now</b></h4>
<li>
cd into the directory

    cd <venv-name>

</li>
<li>
Clone the repository

    git clone https://gitlab.com/janvi.intern/data-growth-prediction.git

</li>

<h4><b>Installing the requirements individually by their latest version</b></h4>
    
    pip install pandas
    pip install influxdb
    pip install influxdb-client
    pip install matplotlib
    pip install plotly
    pip install fpdf
    pip install pystan==2.19.1.1
    pip install fbprophet

<center>OR</center>

<h4><b>Install the dependencies either by installing it from the requirements.txt</b></h4>
    
    pip install -r requirements.txt

<h5><b> NOTE: To install Prophet , Please note that you atleast require 4GB of memory to install prophet, and at least 2GB of memory to use prophet.</b></h5>


<h4><b>Run the jupyter notebook</b></h4>

To run the notebook we use the following command:

<li>
if you are running it on a Kernel-based Machines

    jupyter notebook --ip=0.0.0.0 --port=8888 --no-browser &

</li>
<li>
if you are running it on a GUI-Based Machines
    
    jupyter notebook

</li>
</ul>

<p><h3>Before Running the Project , change the influxdb username and password with the username and password that you have set for influxdb. Once , you are done with changing it, You're good to go. Now Run all the cells of the ipynb file and get the desired output of the project. Also, Create atleast one or more continuous queries if you are running the project for the first time to get the predicted values. 
</h3></p>

</ul>
