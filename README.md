# Data Growth Prediction

<b>Problem statement:</b><br>
The Problem statement we are trying to solve is that There is point where the threshold of the disk will pass and at that time it will be required to add more nodes or add more clusters to avoid the Outage. So , The System will be predicting the data taking the dependent variables prediction in consideration and provide an idea before so that the outage isk can be prevented. 

<b>Approach Followed:</b><br>
Performs Prophet Algorithm to predict the future values of the dependent metrics and generate the analytical and prediction report of the data. 

# Languages / Libraries Used
<ul>

<li>Influxdb</li>

<li>Pandas</li>

<li>FPDF</li>

<li>Prophet</li>

<li>Matplotlib</li>

<li>Plotly</li>

<li>Jupyter Notebook</li>


# Project Tasks 

<li>Import Data from the csv and write it into the influxdb Database. </li>

<li>Convert the Data from the influxdb into a pandas dataframe.</li>

<li>Perform operations to find the rate of growth change on hourly , daily , weekly , 15days and Monthly and plot on the graph.</li>

<li>Downsample the Data on Daily and weekly basis by creating continuous queries. </li>

<li>Generate Data Analytics Reports. </li>

<li>Predicting the future values for univariate (i.e independent metrics )with the help of prophet algorithm.</li>

<li>Predicting the future values for multivariate (i.e dependent metrics )with the help of prophet algorithm with regressors.</li>

<li>Generate the Report for Prediction made with the help of the Prophet algorithm. </li> 

# SETUP

To setup this gitlab repository , for the explainatory results read the .md files in the SETUP directory and to Directly install the repository run the bash script along with reading the script readme.  

# Sample Input

<img src='./sample_input/Screenshot_2022-05-22_at_9.55.36_PM.png'></img>

# Future Enhancement 
<ul>
<li>Handling Non-Stationary Data & Sudden Spikes.</li>

<li>Alert Messages or Notifications when the predicted value is above the Threshold.</li>

</ul>










